//
//  AppDelegate.m
//  MG
//
//  Created by Tim Debo on 5/19/14.
//
//  Modified by blackosx - August/September 2016
//  Added Sparkle Framework - September 2016

#import "AppDelegate.h"
#import "WindowController.h"

@implementation AppDelegate

- (void)applicationWillFinishLaunching:(NSNotification *)aNotification
{

}

-(BOOL)applicationShouldHandleReopen:(NSApplication*)application
                   hasVisibleWindows:(BOOL)visibleWindows{
    if(!visibleWindows){
        [self.windowController.window makeKeyAndOrderFront: nil];
    }
    return YES;
}

- (void) applicationDidFinishLaunching:(NSNotification *)notification {
    self.windowController = [[WindowController alloc] initWithURL: kStartPage];
    [self.windowController setWindowParams];
    [self.windowController showWindow:self];
    [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];

    // blackosx added to run bash script on launch
    NSTask *task = [[NSTask alloc] init];
    NSString *taskPath =
    [NSString stringWithFormat:@"%@/%@",[[NSBundle mainBundle] resourcePath], @"public/bash/script.sh"];
    [task setLaunchPath: taskPath];
    [task launch];
    
    // blackosx create a directory in tmp
    NSFileManager *filemgr;
    filemgr = [NSFileManager defaultManager];
    NSURL *newDir = [NSURL fileURLWithPath:@"/tmp/DarwinDumper"];
    [filemgr createDirectoryAtURL: newDir withIntermediateDirectories:YES attributes:nil error:nil];
    
    // Changing to a Different Directory
    NSString *currentpath;
    filemgr = [NSFileManager defaultManager];
    currentpath = [filemgr currentDirectoryPath];
    NSLog (@"Current directory is %@", currentpath);
    if ([filemgr changeCurrentDirectoryPath: @"/tmp/DarwinDumper"] == NO)
    NSLog (@"Cannot change directory.");
    currentpath = [filemgr currentDirectoryPath];
    NSLog (@"Current directory is %@", currentpath);
    
    // Set log path
    // http://stackoverflow.com/questions/3184235/how-to-redirect-the-nslog-output-to-file-instead-of-console
    NSString *logPath = [currentpath stringByAppendingPathComponent:@"jsToBash"];
    freopen([logPath fileSystemRepresentation],"a+",stderr);
    
    // blackosx - create file to act as stack for bash to send messages to javascript
    [[NSFileManager defaultManager] createFileAtPath:@"/tmp/DarwinDumper/bashToJs" contents:nil attributes:nil];
}


#pragma mark Sparkle updater
- (IBAction)openSparklePref:(id)sender
{
  dispatch_async(dispatch_get_main_queue(), ^{
    [[NSApp mainWindow] beginSheet:self.SparkleWindow completionHandler:^(NSModalResponse returnCode) {
      
    }];
  });
}

- (IBAction)endSparklePref:(id)sender
{
  dispatch_async(dispatch_get_main_queue(), ^{
    [[[NSApp mainWindow] sheetParent] endSheet:self.SparkleWindow returnCode:NSModalResponseOK];
    [self.SparkleWindow  orderOut:self];
  });
}


/*
- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center
     shouldPresentNotification:(NSUserNotification *)notification
{
    return YES;
}
*/

// added to quit application on window close.
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
  NSLog (@"App is closing");
  return YES;
}

@end
