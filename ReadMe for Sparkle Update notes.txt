Important Update Note regarding Sparkle updates
===============================================

Sparkle matches against CFBundleVersion for updates.
However, version shown to the user is CFBundleShortVersionString.

Therefore, you have to update ‘Identity -> Build’ in Xcode (CFBundleVersion) for an update to be recognised.
But you should also change ‘Identity -> Version’ in Xcode (CFBundleShortVersionString) so user recognises version increment.


Example 1
=========

Current App:
Identity -> Version = 3.0.0
Identity -> Build   = 0129

New Release:
Identity -> Version = 3.0.0
Identity -> Build   = 0130

Result = Sparkle will notify of update.


Example 2
=========

Current App:
Identity -> Version = 3.0.0
Identity -> Build   = 0129

New Release:
Identity -> Version = 3.0.1
Identity -> Build   = 0129

Result = Sparkle will not notify of available update.


Suggested practice:
==================

Current App:
Identity -> Version = 3.0.0
Identity -> Build   = 0300

New Release:
Identity -> Version = 3.0.1
Identity -> Build   = 0301

Result = Sparkle will notify of available update.

So, keep Version and Build the same.